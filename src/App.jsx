import { HashRouter as Router } from "react-router-dom";
import Main from "./Main/Main";
import './Assets/Styles/general.scss'

function App() {
  return (
    <Router>
      <div className="App">
        <Main />
      </div>
    </Router>
  );
}

export default App;
