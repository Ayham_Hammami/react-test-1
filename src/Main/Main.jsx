import { lazy, Suspense } from "react";
import { Route, Routes, Navigate } from "react-router";
import Footer from "../Components/Layouts/Footer/Footer";
import Navbar from "../Components/Layouts/Navbar/Navbar";
import Home from "../Pages/HomePage/Home";
import 'atropos/css'

// For Code Spliting
const Products = lazy(() => import("../Pages/ProductsPage/Products"));
const Four0Four = lazy(() => import("../Pages/404Page/Four0Four"));
const Offers = lazy(() => import("../Pages/OffersPage/Offers"));
const Register = lazy(() => import("../Pages/RegisterPage/Register"));
const SignIn = lazy(() => import("../Pages/SignInPage/SignIn"));
//////////////////////
const Main = () => {
  return (
    <>
      <Navbar />
      <Suspense fallback={<div>Loading...</div>}>
      <Routes>
        <Route path="/" element={<Navigate to={"./home"} />} />
        <Route path="/home" element={<Home />} />
        <Route path="/products" element={<Products />} />
        <Route path="/offers" element={<Offers />} />
        <Route path="/signIn" element={<SignIn />} />
        <Route path="/register" element={<Register />} />
        <Route path="/*" element={<Four0Four/>} />
      </Routes>
      </Suspense>
      <Footer />
    </>
  );
};
export default Main;
