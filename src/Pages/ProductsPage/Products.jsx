import {  Container } from "@mui/system";
import cont from "../../Assets/Styles/coontainer.module.scss";
import BestOffer from "../../Components/Ui/BestOffer/bestOffer";
import SliderProducts from "../../Components/Ui/SliderProducts/sliderProducs";
const Products = () => {
  return (
    <>
      <Container className={cont.Container} maxWidth={"xl"}>
        <SliderProducts/>
        <BestOffer/>
      </Container>
    </>
  );
};
export default Products;
