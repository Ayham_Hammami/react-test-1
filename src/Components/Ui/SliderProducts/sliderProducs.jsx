import { Box } from "@mui/system";
import styles from "./sliderProducts.module.scss";
import Grid from "@mui/material/Grid";
import { Chip } from "@mui/material";
import QuantityInput from "../../Inputs/QuantityInput/quantityInput";
import RadioInput from "../../Inputs/radioInput/redioInput";
import ColorInput from "../../Inputs/colorInput/colorInput";
import SliderProductImg from "../SliderProductImg/sliderProductImg";
import productImg from '../../../Assets/Images/egg.png'
const SliderProducts = () => {
  return (
    <>
      <Box className={styles.box}>
        <Grid container>
          <Grid item xs={12} md={5}>
            <SliderProductImg img={productImg}/>
          </Grid>
          <Grid item xs={12} md={7}>
            <Grid container>
              <Grid item xs={12}>
                <Chip label="15% OFF" variant="outlined" />
              </Grid>
              <Grid item xs={12}>
                <h5>Category/Men's shower soup</h5>
              </Grid>
              <Grid item xs={12} md={6}>
                <h1 className={styles.title}>SUNSET FLEUR</h1>
              </Grid>
              <Grid item xs={12} md={6}>
                <Grid container>
                  <Grid item xs={12}>
                    <del className={styles.deleted}>10,000SYP</del>
                  </Grid>
                  <Grid item xs={12}>
                    <div className={styles.price}>10,000SYP</div>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} md={6}>
                <div className={styles.text}>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Soluta excepturi expedita beatae et accusamus impedit,
                  voluptatem eius quos obcaecati recusandae.
                </div>
              </Grid>
              <Grid item xs={12} md={5}>
                <Chip
                  sx={{ width: 100 }}
                  label="Clickable"
                  variant="outlined"
                  onClick={() => {}}
                />
              </Grid>
            </Grid>
            <Grid container>
                <Grid sx={{my:2}} item xs={12}>PRODUCT DETAILES</Grid>
                <Grid sx={{my:2}} item xs={12}><QuantityInput/></Grid>
                <Grid sx={{my:2}} item xs={12}><RadioInput props={['small', 'mid' , 'large' ]}/></Grid>
                <Grid sx={{my:2}} item xs={12}><ColorInput colors={['red','blue','yellow','black','green']}/></Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
export default SliderProducts;
