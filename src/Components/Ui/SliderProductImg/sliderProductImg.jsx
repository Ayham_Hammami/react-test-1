import Grid from "@mui/material/Grid";
import styles from './sliderimg.module.scss'

const SliderProductImg = ({ img }) => {
  return (
    <>
      <Grid className={styles.cont} container>
        <Grid className={styles.cont} item  xs={12}>
          <img className={styles.img} src={img} alt={"..."} />
        </Grid>
        <Grid className={styles.cont} item xs={3}>
          <img className={styles.img} src={img} alt={"..."} />
        </Grid>
        <Grid className={styles.cont} item xs={3}>
          <img className={styles.img} src={img} alt={"..."} />
        </Grid>
        <Grid className={styles.cont} item xs={3}>
          <img className={styles.img} src={img} alt={"..."} />
        </Grid>
        <Grid className={styles.cont} item xs={3}>
          <img className={styles.img} src={img} alt={"..."} />
        </Grid>
      </Grid>
    </>
  );
};
export default SliderProductImg;
