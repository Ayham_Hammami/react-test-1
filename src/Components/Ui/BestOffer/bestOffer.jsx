import { Box } from "@mui/system";
import styles from "./bestOffer.module.scss";
import Atropos from "atropos/react";
import Grid from "@mui/material/Grid";
import OutlinedButton from "../../Buttons/outlinedButton/outLinedButton";
import productImg from '../../../Assets/Images/product.png'

const BestOffer = () => {
  return (
    <>
      <Box className={styles.box}>
        <Atropos className={styles.atropos}>
          <Grid container className={styles.grid}>
            <Grid className={styles.detailes} item xs={12} md={6}>
              <div className={styles.time}>3 Days Left</div>
              <div className={styles.title}>
                Best Offer <br /> As yu need it
              </div>
              <div className={styles.text}>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                Temporibus, voluptate adipisci vero repellat qui sit excepturi
                voluptatem quaerat asperiores iusto corrupti natus beatae nobis
                illum ex fugiat commodi labore ipsa!
              </div>
              <OutlinedButton text={"get the offer"} />
            </Grid>
            <Grid item xs={12} md={6}>
              <div className={styles.productBox}>
                <img className={styles.img} src={productImg} alt={'...'} />
                <div className={styles.price}>5000SYP</div>
                <del className={styles.oldPrice}>5000SYP</del>
              </div>
            </Grid>
          </Grid>
        </Atropos>
      </Box>
    </>
  );
};
export default BestOffer;
