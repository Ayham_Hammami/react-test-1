import styles from "./inlinedButton.module.scss";
const InlinedButton = ({ text }) => {
  return (
    <>
      <button className={styles.InlinedButton}>{text}</button>
    </>
  );
};
export default InlinedButton;
