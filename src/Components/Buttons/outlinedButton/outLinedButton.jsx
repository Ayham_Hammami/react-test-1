import styles from "./outlinedButton.module.scss";
const OutlinedButton = ({ text }) => {
  return (
    <>
      <button className={styles.outlinedButton}>{text}</button>
    </>
  );
};
export default OutlinedButton;
