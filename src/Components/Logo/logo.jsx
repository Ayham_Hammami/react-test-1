import styles from "./logo.module.scss";
const Logo = ({ src }) => {
  return (
    <>
      <img  className={styles.logo} src={src} alt="..." />
    </>
  );
};
export default Logo;
