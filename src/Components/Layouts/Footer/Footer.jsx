import { Box, Container } from "@mui/system";
import Grid from "@mui/material/Grid";
import styles from "./footer.module.scss";
import Logo from "../../Logo/logo";
import whitelogo from "../../../Assets/Svg/logoWhite.svg";
import TelegramIcon from '@mui/icons-material/Telegram';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
const Footer = () => {
  return (
    <>
      <div className={styles.footer}>
        <Container maxWidth={"xl"} sx={{ alignItems: "center", height: "100%" }}>
          <Box sx={{ alignItems: "center", height: "100%" }}>
            <Grid container  sx={{ alignItems: "center" , justifyContent : 'center', height: "100%" }}>
              <Grid div xs={12} md={4}>
                <div className={styles.flexBox}>
                  <Logo src={whitelogo} />
                  <p className={[styles.text]}>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Minus consequuntur magnam quo id, est distinctio quasi
                  </p>
                </div>
              </Grid>
              <Grid div xs={6} md={2}>
                <div className={styles.flexBox}>
                  <div className={styles.title}>Quick Access</div>
                  <div className={styles.text}>Home</div>
                  <div className={styles.text}>Offers</div>
                  <div className={styles.text}>Products</div>
                  <div className={styles.text}>About</div>
                </div>
              </Grid>
              <Grid div xs={6} md={3}>
                <div className={styles.flexBox}>
                  <div className={styles.title}>Community</div>
                  <div className={styles.text}>Give your feedback</div>
                  <div className={styles.text}>explore</div>
                  <div className={styles.text}>Ask a question</div>
                </div>
              </Grid>
              <Grid div xs={6} md={2}>
              <div className={styles.flexBox}>
                  <div className={styles.title}>Contacts</div>
                  <div className={styles.text}>Tell Free : +963 951 645 425</div>
                  <div className={styles.text}>(6:AM To 5:PM)</div>
                  <div className={styles.text}>Email : ayham@gmail.com</div>
                  <div className="group-icons">
                    <LinkedInIcon className={styles.icon}/>
                    <FacebookIcon className={styles.icon}/>
                    <TelegramIcon className={styles.icon}/>
                  </div>
                </div>
              </Grid>
              <Grid div className={styles.copy_right} xs ={12} >Copy Right : Right Reverved 2022</Grid>
            </Grid>
          </Box>
        </Container>
      </div>
    </>
  );
};

export default Footer;
