import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import MenuItem from "@mui/material/MenuItem";
import logo from "../../../Assets/Svg/logo1.svg";
import Logo from "../../Logo/logo";
import { NavLink } from "react-router-dom";
import styles from "./navbar.module.scss";
import InlinedButton from "../../Buttons/inlinedButton/inlinedButton";
const pages = ["signIn", "offers", "products", "home"];

const Navbar = () => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  return (
    <AppBar position="fixed" color={""}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Typography
            variant="img"
            noWrap
            href="/"
            sx={{
              my: 1,
              display: { xs: "none", md: "flex" },
            }}
          >
            <Logo src={logo} />
          </Typography>
          <Box
            className={styles.menuList}
            sx={{
              display: { xs: "none", md: "flex" },
            }}
          >
            {pages.map((page) => (
              <Button
                key={page}
                onClick={handleCloseNavMenu}

                className={styles.link}
              >
                <NavLink className={styles.navlink} to={"/" + page}>
                  {page}
                </NavLink>
              </Button>
            ))}
          </Box>
          {/* Small Screens */}
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page} onClick={handleCloseNavMenu}>
                  <Typography textAlign="center">{page}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Typography
            variant="img"
            noWrap
            component="a"
            href="/"
            sx={{
              display: { xs: "flex", md: "none" },
              my: 1,
              flexGrow: 1,
              justifyContent: 'center'
            }}
          >
            <Logo src={logo} />
          </Typography>
          <InlinedButton text={'Becoome A Member'}/>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Navbar;
