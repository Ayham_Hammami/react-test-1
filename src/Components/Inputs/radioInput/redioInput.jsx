import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import styles from "../input.module.scss";

const RadioInput = ({ props }) => {
  return (
    <>
      <div className={styles.input}>
        <div>Size : </div>
        <RadioGroup
          row
          sx={{ml:5}}
          aria-labelledby="demo-row-radio-buttons-group-label"
          name="row-radio-buttons-group"
        >
          {props.map((i) => {
            return (
              <>
                <FormControlLabel
                  value={i}
                  control={<Radio />}
                  label={i}
                />
              </>
            );
          })}
        </RadioGroup>
      </div>
    </>
  );
};
export default RadioInput;
