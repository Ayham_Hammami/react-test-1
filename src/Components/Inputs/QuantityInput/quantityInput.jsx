import { useState } from "react";
import styles from '../input.module.scss'

const QuantityInput = () => {
  let [conter, setCounter] = useState(0);
  return (
    <>
      <div
       className={styles.input}
      >
        <div>Quantity :</div>
        <button
          onClick={() => {
            if(conter !==0)
            setCounter(conter - 1);
          }}
        >
          -
        </button>
        <div>{conter}</div>
        <button
          onClick={() => {
            setCounter(conter + 1);
          }}
        >
          +
        </button>{" "}
      </div>
    </>
  );
};
export default QuantityInput;
