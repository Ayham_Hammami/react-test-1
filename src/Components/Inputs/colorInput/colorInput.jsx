import { Radio } from "@mui/material";
import { blue, pink, yellow } from "@mui/material/colors";
import { useState } from "react";
import styles from "../input.module.scss";

const ColorInput = ({ colors }) => {
  const [selectedValue, setSelectedValue] = useState("a");

  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };

  const controlProps = (item) => ({
    checked: selectedValue === item,
    onChange: handleChange,
    value: item,
    name: "color-radio-button-demo",
    inputProps: { "aria-label": item },
  });

  return (
    <div className={styles.input}>
    <div>Color</div>
      {colors.map((color) => {
        return (
          <>
            <Radio
              {...controlProps(color)}
              sx={{
                color: color,
                ml:2,
                "&.Mui-checked": {
                  color: color,
                  
                },
              }}
            />
          </>
        );
      })}
    </div>
  );
};
export default ColorInput;
